import React, { Component } from 'react';
import {View, StyleSheet, Text} from 'react-native';
import Routes from './src/Routes';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react'
import { Persistor, Store } from './src/Services/Redux/store';


class App extends Component {
  render() {
    return (
      <Provider store={Store}>
        <PersistGate loading={null} persistor={Persistor}>
          <Routes {...this.props}/>
        </PersistGate>
      </Provider>
    );
  }
}

export default App;

const styles = StyleSheet.create({
  
})

