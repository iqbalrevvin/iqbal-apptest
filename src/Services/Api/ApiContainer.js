import axios from 'axios';

//Base URL API
const BASE_URL = 'https://simple-contact-crud.herokuapp.com'

//Endpoint
export const CONTACT = 'contact'

export default API = axios.create({
    headers: {
        Accept: 'application/json',
        'Access-Control-Allow-Origin' : '*',
        'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS'
    },
    baseURL: BASE_URL,
    timeout:1000,
});