//=========== Contact Reducer ===========
export const SYNC_CONTACT = 'SYNC-CONTACT'
export const ADD_CONTACT = 'ADD-CONTACT'
export const EDIT_CONTACT = 'EDIT-CONTACT'
export const DELETE_CONTACT = 'DELETE-CONTACT'