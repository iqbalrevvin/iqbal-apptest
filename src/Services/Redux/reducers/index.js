import {combineReducers} from 'redux'
import ContactReducer from './contactReducer'


const reducer = combineReducers({
    contact: ContactReducer
})

export default reducer