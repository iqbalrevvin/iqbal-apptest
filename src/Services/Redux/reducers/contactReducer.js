import { ADD_CONTACT, DELETE_CONTACT, EDIT_CONTACT, SYNC_CONTACT } from "../../constant/constantReducer";
import { contacts } from "../../dummyData";


const initialState = {
    data: []
}

const ContactReducer = (state = initialState, action) => {
    switch (action.type) {
        case SYNC_CONTACT:
            return {
                data: action.payload
            }
        case ADD_CONTACT:
            return {
                ...state,
                data: [...state.data, action.payload]
            }
        case EDIT_CONTACT:
            const newDataUpdate = state.data.map(item => item.id == action.payload.id ? action.payload : item)
            return{
                ...state,
                data: newDataUpdate
            }
        case DELETE_CONTACT:
            const newData = state.data.filter(item => item.id != action.payload)
            return{
                ...state,
                data: newData
            }
        default:
            return state;
    }
}

export default ContactReducer