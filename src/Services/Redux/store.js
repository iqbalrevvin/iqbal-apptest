import { applyMiddleware, createStore, compose } from "redux";
import {persistStore, persistReducer} from 'redux-persist'
import AsyncStorage from "@react-native-async-storage/async-storage";
import reducer from "./reducers";

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    blacklist:[''],
    whitelist:['contact']
}

const persistedReducer = persistReducer(persistConfig, reducer)

let composeEnhanchers = compose

if(__DEV__){
    composeEnhanchers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
}

const Store = createStore(persistedReducer, composeEnhanchers(applyMiddleware()))
const Persistor = persistStore(Store)

export {Store, Persistor}