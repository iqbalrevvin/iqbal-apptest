const iconAdd = require('./icon-add.png');
const iconSearch = require('./icon-search.png');
const iconMoreVertical = require('./icon-more-vertical.png');
const iconGoBack = require('./icon-goback.png');
const iconPhone = require('./icon-phone.png');
const iconChat = require('./icon-chat.png');
const iconTelegram = require('./icon-telegram.png');
const iconWhatsapp = require('./icon-whatsapp.png');
const iconEdit = require('./icon-edit.png');
const iconChecklist = require('./icon-checklist.png');
const iconSync = require('./icon-sync.png');

export {
    iconAdd, iconSearch, iconMoreVertical, iconGoBack,
    iconPhone, iconChat, iconTelegram, iconWhatsapp,
    iconEdit, iconChecklist, iconSync
}