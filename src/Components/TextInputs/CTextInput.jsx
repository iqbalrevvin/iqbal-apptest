//import liraries
import React, { useState } from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';
import CText from '../Texts/CText';

// create a component
const CTextInput = ({value, label, placeholder, onChangeText, autoFocus, type, keyboardType, maxLength}) => {
    const [focus, setFocus] = useState(false)
    return (
        <View style={styles.container(focus)}>
            <View style={{ marginBottom:2, marginTop:2 }}>
                <CText color='#6E6E76' size={14} bold>{label}</CText>
            </View>
            <TextInput style={styles.textInputSection} value={value} placeholder={placeholder} autoFocus={autoFocus}
                onChangeText={onChangeText} placeholderTextColor='#6E6E76' onFocus={() => setFocus(true)} 
                onBlur={() => setFocus(false)} type={type} keyboardType={keyboardType} maxLength={maxLength} />
        </View>
    );
};

//make this component available to the app
export default React.memo(CTextInput);

// define your styles
const styles = StyleSheet.create({
    container: (focus) => ({
        flex:1,
        backgroundColor:'ghostwhite',
        paddingHorizontal:15,
        borderRadius: 10,
        borderWidth: focus?2:0,
        borderColor:'dodgerblue'
    }),
    textInputSection:{
       borderWidth:0,
       height: 35,
       padding:0,
       zIndex:10
    }
});

