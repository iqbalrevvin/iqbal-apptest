import React from 'react'
import {StyleSheet, Text} from 'react-native'

const CText = ({children, style, bold, color, size, numberOfLines}) => {
    return(
        <Text style={[style, styles.textSection(bold, color, size)]} numberOfLines={numberOfLines}>
            {children}
        </Text>
    )
}

export default React.memo(CText)

const styles = StyleSheet.create({
    textSection:(bold,color,size) => ({
        fontWeight: bold?'bold':'normal',
        color:color||'black',
        fontSize: size||16
    })
})