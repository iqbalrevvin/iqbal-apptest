import React from 'react'
import {StyleSheet, View, Image, TouchableOpacity} from 'react-native'
import { iconAdd, iconSearch } from '../../Assets/Icons'
import CText from '../Texts/CText'

const CHeader = ({title, iconLeft, iconRight, onPressLeft, onPressRight}) => {
    return(
        <View style={styles.container}>
            <View style={styles.headerContainer}>
                <TouchableOpacity onPress={onPressLeft}>
                    <Image source={iconLeft} style={styles.iconAddSection} />
                </TouchableOpacity>
                <View>
                  <CText bold>{title}</CText>
                </View>
                {onPressRight && (
                    <TouchableOpacity onPress={onPressRight}>
                        <Image source={iconRight} style={styles.iconAddSection} />
                    </TouchableOpacity>
                )}
            </View>
        </View>
    )
}

CHeader.defaultProps = {
    title: 'Title Header',
    iconLeft: iconSearch,
  
}

export default React.memo(CHeader)

const styles = StyleSheet.create({
    container:{
        backgroundColor:'white',
        borderWidth:0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.1,
        shadowRadius: 1,
        elevation: 5,
    },
    headerContainer:{
        padding:15,
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    iconAddSection:{
        width:25,
        height:25
    }
})