//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

// create a component
const CPhoto = ({url, size, circle}) => {
    return (
        <View style={styles.container}>
            <Image source={{uri: url}} style={styles.photoSection(size,circle)} />
        </View>
    );
};

CPhoto.defaultProps = {
    url: 'https://picsum.photos/200/300',
    size:75,
    circle:false
}

//make this component available to the app
export default React.memo(CPhoto);

// define your styles
const styles = StyleSheet.create({
    container: {
        backgroundColor:'lightgray',
        borderRadius: 50
    },
    photoSection:(size, circle) => ({
        width:size,
        height:size,
        borderRadius: circle?size/2:0
    })
});

