import API, { CONTACT } from "../../Services/Api/ApiContainer"

export const addContact = async (data) => {
    try {
        const request = await API.post(CONTACT, data)
        return request
    } catch (error) {
        console.log('AddScreen.action@addContact', error)
    }
}