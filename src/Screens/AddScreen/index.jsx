import React, { Component } from 'react'
import { StyleSheet, View, SafeAreaView, Alert } from 'react-native'
import { connect } from 'react-redux'
import { iconChecklist, iconGoBack } from '../../Assets/Icons'
import CHeader from '../../Components/Headers/CHeader'
import CLoading from '../../Components/Loaders/CLoading'
import CPhoto from '../../Components/Photos/CPhoto'
import CTextInput from '../../Components/TextInputs/CTextInput'
import { ADD_CONTACT, SYNC_CONTACT } from '../../Services/constant/constantReducer'
import { getContact } from '../HomeScreen/action'
import { addContact } from './action'

class AddScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading:false,
            firstName: '',
            lastName: '',
            age: ''
        }
    }

    async componentWillUnmount(){
        
        // await getContact()
    }

    _handleChangeText = (type) => e => {
        this.setState({
            [type]: e
        })
    }

    _handleAddContact = () => {
        const {firstName, lastName, age} = this.state
        if(firstName, lastName, age){
            this.setState({loading:true})
            const parsingData = {firstName, lastName, age}
            addContact({photo: 'http://dummyimage.com/169x100.png/dddddd/000000', ...parsingData}).then(res => {
                console.log('add data', res)
                this.setState({loading:false})
                this._handleSyncContact()
                this.props.navigation.goBack()
            })
            // const data = {
            //     id: generateId(30),
            //     photo: "http://dummyimage.com/169x100.png/dddddd/000000",
            //     ...this.state
            // }
            // this.props.AddContactDispatch(data)
            // this.props.navigation.goBack()
        }else{
            Alert.alert('Fill All Fields', 'Make sure all fields are filled in, do not leave any blanks!')
        }
    }

    _handleSyncContact = async () => {
        try {
            const response = await getContact()
            if(response?.data){
                this.props.SyncContactDispatch(response.data.data)  
            }else{
                Alert.alert('Failed Sync', 'Sync contacts by tapping the sync icon at the top left')
            }
        } catch (error) {
            console.log(error)
            Alert.alert('Failed Sync', 'Failed to sync contacts, make sure you are connected to the internet.')
        }
    }

    render() {
        const { route, navigation } = this.props
        const {loading, firstName, lastName, age} = this.state
        return (
            <SafeAreaView style={styles.container}>
                <CHeader title='Add Contact'
                    iconLeft={iconGoBack} onPressLeft={() => navigation.goBack()} 
                    iconRight={iconChecklist} onPressRight={this._handleAddContact}
                />
                {loading && (
                    <CLoading message='Saving & Sync Contact' />
                )}
                <View style={styles.contentContainer}>
                    <View style={styles.headContentContainer}>
                        <CPhoto circle />
                    </View>
                    <View style={styles.bodyContentContainer}>
                        <View style={styles.formContainer}>
                            <CTextInput value={firstName} label='First Name' placeholder='Fill First Name' 
                                onChangeText={this._handleChangeText('firstName')} autoFocus />
                        </View>
                        <View style={styles.formContainer}>
                            <CTextInput value={lastName} label='Last Name' 
                            placeholder='Fill Last Name' onChangeText={this._handleChangeText('lastName')} />
                        </View>
                        <View style={styles.formContainer}>
                            <CTextInput value={age} label='Age' placeholder='Fill Age' 
                            type='number' onChangeText={this._handleChangeText('age')} 
                            keyboardType='numeric' maxLength={2} />
                        </View>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state) => ({
    contactState: state.contact
})

const mapDispatchToProps = (dispatch) => ({
    AddContactDispatch: (data) => dispatch({type: ADD_CONTACT, payload:data}),
    SyncContactDispatch: (data) => dispatch({type: SYNC_CONTACT, payload:data}),
})

export default connect(mapStateToProps, mapDispatchToProps)(AddScreen)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    contentContainer: {

    },
    headContentContainer: {
        alignItems: 'center',
        marginVertical: 10
    },
    bodyContentContainer: {
        height: '100%',
        backgroundColor: 'transparent',
        margin: 10
    },
    formContainer:{
        flexDirection:'row',
        marginTop:10
    }
})
