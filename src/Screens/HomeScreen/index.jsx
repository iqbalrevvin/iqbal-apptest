import React, { Component, Fragment } from 'react'
import { StyleSheet, View, SafeAreaView, FlatList, Alert, ToastAndroid, Platform } from 'react-native'
import { connect } from 'react-redux'
import { iconAdd, iconSync } from '../../Assets/Icons'
import CHeader from '../../Components/Headers/CHeader'
import CLoading from '../../Components/Loaders/CLoading'
import { DELETE_CONTACT, SYNC_CONTACT } from '../../Services/constant/constantReducer'
import { deleteContact, getContact } from './action'
import CListItem from './Components/CListItem'

class HomeScreen extends Component {
    constructor() {
        super()
        this.state = {
            loading: false,
            loadingMessage: 'Please Wait',
            data: []
        }
    }

    async componentDidMount() {
        const { contactState } = this.props

        if (contactState.data.length == 0) {
            this._handleSyncContact()
        }
    }

    _handleLoadMore = () => {
        console.log('Load More')
    }

    _handlePressMore = (item) => {
        Alert.alert(
            `${item.firstName} ${item.lastName}`,
            'Please Select Action',
            [
                {
                    text: 'Delete',
                    onPress: () => this._confirmDelete(item),
                    style: 'destructive'
                },
                {
                    text: 'Edit',
                    onPress: () => this.props.navigation.navigate('Update', { detail: { url: `https://picsum.photos/id/${item.age * Math.floor(Math.random() * 10)}/200/300`, ...item } }),
                    style: 'default'
                },
                {
                    text: 'Close',
                    onPress: () => console.log("Cancel Pressed"),
                    style: 'default'
                },
            ],
            { cancelable: true }
        );
    }

    _confirmDelete = (item) => {
        Alert.alert(
            `Delete ${item.firstName}`,
            'Are you sure?',
            [
                {
                    text: 'Yes Delete',
                    onPress: () => this._handleDeleteContact(item.id),
                    style: 'destructive'
                },
                {
                    text: 'Cancel',
                    onPress: () => console.log("Cancel Pressed"),
                    style: 'default'
                },
            ],
            { cancelable: true }
        );
    }

    _handleDeleteContact = (id) => {
        deleteContact(id).then((res) => {
            if (!res) {
                if (Platform.OS === 'android') {
                    ToastAndroid.show('Failed comunication with server', ToastAndroid.SHORT)
                } else {
                    Alert.alert('Oops', 'The contact was successfully deleted on your device, but cant communicate to the server')
                }
            } else {
                Alert.alert('Success', 'Contact successfuly delete')
            }
            console.log('HomeScreen.index@handleDeleteContact', res)
        })
        this.props.DeleteContactDispatch(id)
    }

    _confirmSyncContact = () => {
        Alert.alert(
            `Sync Contact`,
            'Sync contacts from the cloud, Are you sure?',
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log("Cancel Pressed"),
                    style: 'default'
                },
                {
                    text: 'Yes Sync',
                    onPress: () => this._handleSyncContact(),
                    style: 'default'
                }
            ],
            { cancelable: true }
        );
    }

    _handleSyncContact = async () => {
        this.setState({ loading: true, loadingMessage: 'Syncing Contacts' })
        try {
            const response = await getContact()
            if (response?.data) {
                this.setState({ loading: false })
                this.props.SyncContactDispatch(response.data.data)
                console.log(response.data)
            } else {
                this.setState({ loading: false })
                Alert.alert('Failed Sync', 'Contact is Empty, Sync contacts by tapping the sync icon at the top left')
            }
        } catch (error) {
            this.setState({ loading: false })
            Alert.alert('Failed Sync', 'Failed to sync contacts, make sure you are connected to the internet.')
        }
    }

    render() {
        const { data } = this.props.contactState
        console.log(data)
        const { loading, loadingMessage } = this.state
        const renderItem = ({ item }) => {
            return (
                <View style={{ marginBottom: 15, marginTop: 10 }}>
                    <CListItem name={`${item.firstName} ${item.lastName}`}
                        info={`Age : ${item.age}`}
                        photoUrl={`https://picsum.photos/id/${item.age * Math.floor(Math.random() * 10)}/200/300`}
                        onPressItem={() => this.props.navigation.navigate('Detail', {
                            detail: { url: `https://picsum.photos/id/${item.age * Math.floor(Math.random() * 10)}/200/300`, ...item }
                        })}
                        onPressMore={() => this._handlePressMore(item)}
                    />
                </View>
            )
        }
        return (
            <Fragment>
                {loading && (
                    <CLoading message={loadingMessage} />
                )}
                <SafeAreaView style={styles.container}>
                    <CHeader title='My Contact' iconLeft={iconSync} iconRight={iconAdd}
                        onPressLeft={this._confirmSyncContact}
                        onPressRight={() => this.props.navigation.navigate('Add')}
                    />
                    {/* <CText>{JSON.stringify(data)}</CText> */}
                    <View style={styles.contentContainer}>
                        <FlatList data={data} keyExtractor={(item => item.id.toString())}
                            onEndReached={this._handleLoadMore} onEndReachedThreshold={0.5}
                            initialNumToRender={7} renderItem={(renderItem)}
                            showsVerticalScrollIndicator={false}
                            ListHeaderComponent={<View></View>}
                        />
                    </View>
                </SafeAreaView>
            </Fragment>
        )
    }
}

const mapStateToProps = (state) => ({
    contactState: state.contact
})

const mapDispatchToProps = (dispatch) => ({
    SyncContactDispatch: (data) => dispatch({ type: SYNC_CONTACT, payload: data }),
    DeleteContactDispatch: (id) => dispatch({ type: DELETE_CONTACT, payload: id })
})

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    contentContainer: {
        flex: 1,
        marginHorizontal: 10,
    }
})
