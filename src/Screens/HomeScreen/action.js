import API, { CONTACT } from "../../Services/Api/ApiContainer"

export const getContact = async () => {
    try {
        const request = await API.get(CONTACT)
        return request
    } catch (error) {
        console.log('HomeScreen.action@getContact', error)
    }
}
export const deleteContact = async (id) => {
    try {
        const request = await API.delete(`${CONTACT}/${id}`)
        return request
    } catch (error) {
        console.log('HomeScreen.action@deleteContact', error)
    }
}