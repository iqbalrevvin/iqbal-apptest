//import liraries
import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Dimensions } from 'react-native';
import { iconMoreVertical } from '../../../Assets/Icons';
import CPhoto from '../../../Components/Photos/CPhoto';
import CText from '../../../Components/Texts/CText';

// create a component
const {width} = Dimensions.get('window')
const CListItem = ({photoUrl, name, info, onPressItem, onPressMore}) => {
    return (
        <View style={styles.container}>
            <View style={styles.contentContainer}>
                <TouchableOpacity style={{ flexDirection:'row' }} onPress={onPressItem}>
                    <CPhoto circle size={50} url={photoUrl} />
                    <View style={{ paddingHorizontal:10, paddingVertical:5 }}>
                        {}
                        <CText bold numberOfLines={1} style={{ width:width-200 }}>{name}</CText>
                        <CText>{info}</CText>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={onPressMore}>
                    <Image source={iconMoreVertical} style={{ width:20, height:20 }} />
                </TouchableOpacity>
            </View>
        </View>
    );
};

CListItem.defaultProps = {
    photoUrl: 'https://picsum.photos/200/300',
    name: 'Martin Sutejo',
    info: 'Age : 20',
    onPressItem: () => alert('On Press Item'),
    onPressMore: () => alert('On Press More')
}

const compare = (prevProps, nextProps) => {
    return JSON.stringify(prevProps) === JSON.stringify(nextProps)
}

export default React.memo(CListItem, compare);

const styles = StyleSheet.create({
    container: {
      
    },
    contentContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    }
});

