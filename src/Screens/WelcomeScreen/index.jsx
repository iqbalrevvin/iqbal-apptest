import React, { Component } from 'react'
import { Text, StyleSheet, View, ActivityIndicator } from 'react-native'
import CText from '../../Components/Texts/CText'

class WelcomeScreen extends Component {
    componentDidMount(){
        setTimeout(() => {
            this.props.navigation.navigate('Home')
        },1000)
    }
    render() {
        return (
            <View style={styles.container}>
                <CText bold> Please Wait </CText>
                <View style={{ top:10 }}>
                    <ActivityIndicator color='black' />
                </View>
            </View>
        )
    }
}

export default WelcomeScreen

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white',
        alignItems:'center',
        justifyContent:'center'
    }
})
