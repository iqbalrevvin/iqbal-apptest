import React, { Component } from 'react'
import { StyleSheet, View, SafeAreaView, Image } from 'react-native'
import { iconChat, iconEdit, iconGoBack, iconPhone, iconTelegram, iconWhatsapp } from '../../Assets/Icons'
import CHeader from '../../Components/Headers/CHeader'
import CPhoto from '../../Components/Photos/CPhoto'
import CText from '../../Components/Texts/CText'

class DetailScreen extends Component {
    render() {
        const { route, navigation } = this.props
        const { detail } = route.params
        return (
            <SafeAreaView style={styles.container}>
                <CHeader title={`${detail.firstName}`} iconLeft={iconGoBack} iconRight={iconEdit} 
                    onPressLeft={() => navigation.goBack()} onPressRight={() => this.props.navigation.navigate('Update', {detail:{url:`https://picsum.photos/id/${detail.age * Math.floor(Math.random() * 10)}/200/300`, ...detail}})} />
                <View style={styles.contenContainer}>
                    <View style={styles.headContentContainer}>
                        <CPhoto url={detail.url} circle />
                        <CText bold style={{ marginTop:18 }}>{detail.firstName} {detail.lastName}</CText>
                        <CText size={12} color='darkgray' bold>{detail.age} old</CText>
                    </View>
                    <View style={styles.bodyContentContainer}>
                        <View style={styles.infoItemContainer}>
                            <CText bold>+62 812-2314-2314</CText>
                            <View style={{ flexDirection:'row', justifyContent:'space-between', alignItems:'center', width:75 }}>
                                <Image source={iconPhone} style={styles.iconSection} />
                                <Image source={iconChat} style={styles.iconSection} />
                            </View>
                        </View>
                        <View style={[styles.infoItemContainer, {marginTop:10}]}>
                            <CText bold>Telegram</CText>
                            <View style={styles.infoItemContainer}>
                                <Image source={iconTelegram} style={styles.iconSection} />
                            </View>
                        </View>
                        <View style={styles.infoItemContainer}>
                            <CText bold>WhatsApp</CText>
                            <View style={styles.iconInfoContainer}>
                                <Image source={iconWhatsapp} style={styles.iconSection} />
                            </View>
                        </View>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

export default DetailScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    contentContainer: {
        
    },
    headContentContainer:{
        alignItems:'center',
        marginVertical:10
    },
    bodyContentContainer:{
        padding:20
    },
    infoItemContainer:{
        flexDirection:'row', 
        justifyContent:'space-between', 
        alignItems:'center',
        marginBottom:25
    },
    iconInfoContainer:{
        flexDirection:'row', 
        justifyContent:'space-between', 
        alignItems:'center'
    },
    iconSection:{
        width:25,
        height:25
    }

})
