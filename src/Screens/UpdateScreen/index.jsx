import React, { Component } from 'react'
import { StyleSheet, View, SafeAreaView } from 'react-native'
import { connect } from 'react-redux'
import { iconChecklist, iconGoBack } from '../../Assets/Icons'
import CHeader from '../../Components/Headers/CHeader'
import CPhoto from '../../Components/Photos/CPhoto'
import CTextInput from '../../Components/TextInputs/CTextInput'
import CText from '../../Components/Texts/CText'
import { EDIT_CONTACT } from '../../Services/constant/constantReducer'
import { updateContact } from './actions'

class UpdateScreen extends Component {
    constructor(props) {
        super(props)
        const { detail } = this.props.route.params
        this.state = {
            firstName: detail.firstName,
            lastName: detail.lastName,
            age: detail.age.toString()
            
        }
    }

    _handleChangeText = (type) => e => {
        this.setState({
            [type]: e
        })
    }

    _handleUpdateContact = () => {
        const { detail } = this.props.route.params
        const data = {
            id: detail.id,
            photo: detail.photo,
            ...this.state
        }
        updateContact(detail.id, {...this.state, photo: detail.photo}).then(res => console.log(res))
        this.props.EditContactDispatch(data)
        this.props.navigation.navigate('Home')
    }

    render() {
        const { route, navigation } = this.props
        const { detail } = route.params
        const {firstName, lastName, age} = this.state
        return (
            <SafeAreaView style={styles.container}>
                <CHeader title={`${detail.firstName} ${detail.lastName}`} 
                    iconLeft={iconGoBack} onPressLeft={() => navigation.goBack()} 
                    iconRight={iconChecklist} onPressRight={this._handleUpdateContact}
                />
                <View style={styles.contentContainer}>
                    <CText>{JSON.stringify(this.state.formInput)}</CText>
                    <View style={styles.headContentContainer}>
                        <CPhoto url={detail.url} circle />
                    </View>
                    <View style={styles.bodyContentContainer}>
                        <View style={styles.formContainer}>
                            <CTextInput value={firstName} label='First Name' placeholder='Fill First Name' 
                                onChangeText={this._handleChangeText('firstName')} autoFocus />
                        </View>
                        <View style={styles.formContainer}>
                            <CTextInput value={lastName} label='Last Name' 
                            placeholder='Fill Last Name' onChangeText={this._handleChangeText('lastName')} />
                        </View>
                        <View style={styles.formContainer}>
                            <CTextInput value={age} label='Age' placeholder='Fill Age' 
                            type='number' onChangeText={this._handleChangeText('age')} 
                            keyboardType='numeric' maxLength={2} />
                        </View>
                    </View>
                    
                </View>
            </SafeAreaView>
        )
    }
}

const mapDispatchToProps = (dispatch) => ({
    EditContactDispatch: (item) => dispatch({type: EDIT_CONTACT, payload:item})
})

export default connect(null, mapDispatchToProps)(UpdateScreen)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    contentContainer: {

    },
    headContentContainer: {
        alignItems: 'center',
        marginVertical: 10
    },
    bodyContentContainer: {
        height: '100%',
        backgroundColor: 'transparent',
        margin: 10,
     
    },
    formContainer:{
        flexDirection:'row',
        marginTop:10
    }
})
