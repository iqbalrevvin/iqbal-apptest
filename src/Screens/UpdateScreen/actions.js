import API, { CONTACT } from "../../Services/Api/ApiContainer"

export const updateContact = async (id, data) => {
    try {
        const url = `${CONTACT}/${id}`
        const request = await API.put(url, data)
        return request
    } catch (error) {
        console.log('UpdateScreen.action@updateContact', error)
    }
}