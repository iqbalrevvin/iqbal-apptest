import React from 'react'
import {createNativeStackNavigator} from '@react-navigation/native-stack'
import WelcomeScreen from '../Screens/WelcomeScreen'
import HomeScreen from '../Screens/HomeScreen'
import DetailScreen from '../Screens/DetailScreen'
import UpdateScreen from '../Screens/UpdateScreen'
import AddScreen from '../Screens/AddScreen'

const screenOptionsConfig = {
    headerShown: false,
    gestureEnabled: true,
    gestureDirection: 'horizontal',
}

const Stack = createNativeStackNavigator()
const StackNavigator = (props) => {
    return(
        <Stack.Navigator screenOptions={screenOptionsConfig} animation={true}>
            <Stack.Screen name='Welcome' component={WelcomeScreen} />
            <Stack.Screen name='Home' component={HomeScreen} {...props} />
            <Stack.Screen name='Detail' component={DetailScreen} />
            <Stack.Screen name='Update' component={UpdateScreen} />
            <Stack.Screen name='Add' component={AddScreen} />
        </Stack.Navigator>
    )
}

export default StackNavigator